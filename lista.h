/*


*/
#define  MAX_ILOSC_ZNAKOW 20
/*==============================================================================
						structures
===============================================================================*/


struct wezel{			  /* typ wezel */

   char nazwisko[MAX_ILOSC_ZNAKOW];
   struct wezel  *nastepny;
   struct wezel  *poprzedni;
};

typedef struct wezel* p_wezel;

typedef struct {		  /* typ kolejka */
   p_wezel  poczatek;
   p_wezel	wybrany;
   p_wezel  koniec;
}lista;
/*==============================================================================
						methodes
===============================================================================*/
 
void odczyt_z_pliku(lista *pk );
int usun_kontrahenta(lista *pk ); 	  
void dodaj_kontrahenta(lista *pk); /* dodaj na koniec kolejki   */
void wypisanie_listy(lista *pk );	       /* wypisz wszystkich kontrahentow	    */
void zapisz_do_pliku(lista *pk );
void nowa_lista(lista *pk );
void sortowanie_listy(lista *pk );
