#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#include "lista.h"

enum MENU {N, S, L, W, Z, O, U, K};
/*
N - nowy kontahent
S - sortowanie
L - nowa lista
W - wypisanie listy
U - usuwanie
K - koniec
*/

/* program glowny ---------------------------------------------------------- */

void main()
{
     lista *pk = malloc(sizeof(lista)); /* inicjalizacja kolejki */
	 
	 int  poprwane_wczytywanie;
	 enum MENU zmienna_pom_menu;
     pk->koniec = NULL;	
	 pk->wybrany = NULL;		
	 pk->poczatek = NULL;	   /* lista pusta */
     poprwane_wczytywanie=0;

	do
	{
		printf("\n\t\tMENU GLOWNE\n\n");	
		printf("\t0 - nowy kontahent\n");
		printf("\t1 - sortowanie\n");
		printf("\t2 - nowa lista\n");
		printf("\t3 - wypisanie listy\n");
		printf("\t4 - zapisanie listy do pliku\n");
		printf("\t5 - odczyt listy z pliku\n");
		printf("\t6 - usuwanie\n");
		printf("\t7 - koniec\n");

		poprwane_wczytywanie=scanf("%d",&zmienna_pom_menu); // moze getch()
		if(poprwane_wczytywanie!=0) 
		{			
			switch (zmienna_pom_menu)
			{
				case S:
				{
					printf("\tS - sortowanie\n");
					sortowanie_listy( pk );
				}break;
				case N:
				{
					printf("\tN - nowy kontahent:\n");
					fflush(stdin);
					dodaj_kontrahenta( pk ); 
				}
				break;

				
				case L:
				{
					printf("\tL - nowa lista\n");
					nowa_lista( pk );
				}
				break;				
				case W:
				{
					printf("\tW - wypisanie listy\n");
					wypisanie_listy( pk );
				}
				break;
				case Z:
				{
					printf("\tZ - zapis do pliku\n"); // dopisanie
					zapisz_do_pliku( pk );
				}
				break;
				case O:
				{
					printf("\tO - odczyt z pliku\n");// odczyt jako nowy lub dopis
					odczyt_z_pliku( pk );
				}
				break;
					
				case U:
				{
					printf("\tU - usuwanie\n");
					usuwa_wybrany_element( pk );
				}
				break;
				case K:
				{
					printf("\tK - koniec\n");
					system("pause");
					break;
				}
				default:
				{
				}
				break;	 
			}
		}
		else if (poprwane_wczytywanie!=1)
		fflush(stdin);
		
	}while(zmienna_pom_menu!=7);
	
	free(pk);
}

