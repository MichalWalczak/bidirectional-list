#include "lista.h"

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>


void zwolnijwezel( p_wezel p );	       /* zwolnienie wezla	    */
void usuwa_wybrany_element(lista *pk);      

p_wezel wyszukaj_kontrahenta(lista *pk, char *);
p_wezel rezerwuj_pamiec( void );		       /* rezerwuje pamiec    */

/* Funkcja pomocnicza------------------------------------------------------- */

p_wezel  rezerwuj_pamiec( void )	/* zwraca adres nowego wezla */
{
   p_wezel  p;
   p = (p_wezel) malloc( sizeof( struct wezel ) ) ;
   return( p );
}

/* Funkcja pomocnicza------------------------------------------------------- */

void zwolnijwezel( p_wezel p )
{
   free( p );
   return;
}
/* ------------------------------------------------------------------------- */

void dodaj_kontrahenta( lista *pk )	  /* dodaj na koniec kolejki */
{
    p_wezel  pom, tmp;
    char bufor[MAX_ILOSC_ZNAKOW+1];
 	tmp = rezerwuj_pamiec();		/* rezerwacja pamieci    */

	printf("\nnazwisko: ");
	fgets(bufor, MAX_ILOSC_ZNAKOW, stdin);
	strcpy(tmp->nazwisko, bufor);	

   	tmp->nastepny = NULL;		    /* nowy koniec listy     */
   	tmp->poprzedni = NULL;
   	if ( pk->poczatek == NULL )
   	{
    	pk->poczatek = tmp;		    /* tylko na poczatku       */
    	pk->koniec=tmp;
   	}
	else 							/* potem		       */
   	{
		pom = pk->poczatek;
		while (pom->nastepny != NULL)
			   pom = pom->nastepny;
		tmp->poprzedni = pom;
		pom->nastepny = tmp;
		pk->koniec=tmp;
	}
}

//------------------------------------------------------------------------
void odczyt_z_pliku(lista *pk )
{
    FILE *wczytaj = NULL;
    char *nazwa_pliku, bufor[MAX_ILOSC_ZNAKOW + 1];
	p_wezel tmp, pom;

    printf("\nPodaj nazwe pliku: ");
    fflush(stdin);
	fgets(bufor, MAX_ILOSC_ZNAKOW, stdin);
    nazwa_pliku = (char*)malloc(sizeof(char)* (strlen(bufor) + 1));
    strncpy(nazwa_pliku, bufor,strlen(bufor) - 1);    
	strcat(nazwa_pliku, ".txt");
	printf("%s",nazwa_pliku);
     
    wczytaj = fopen(nazwa_pliku, "r");
    if (wczytaj == NULL)
    printf("\nBlad otwarcia pliku");
    else
	{
	    while (fscanf(wczytaj, "%s", bufor) != EOF)
		{
			strcat(bufor, "\n");
		    //alokowanie pamieci na nowy element
		    tmp = rezerwuj_pamiec();
		    tmp->nastepny = NULL;
		    tmp->poprzedni = NULL;
		    strcpy(tmp->nazwisko, bufor); //wczytywanie nazwiska
		     
		    if (pk->poczatek == NULL)
		    pk->poczatek = tmp;
		    else
		    {
		    	pom = pk->poczatek;
		    	while (pom->nastepny != NULL)
		    	pom = pom->nastepny;
		    	tmp->poprzedni = pom;
		    	pom->nastepny = tmp;
		    	
		    }
	    }
	    printf("\nPlik zostal wczytany");
	}
    fclose(wczytaj);
}

//------------------------------------------------------------------------- 
void zapisz_do_pliku(lista *pk )
{
    FILE *zapisz = NULL;
    p_wezel tmp;
    char *nazwa_pliku, bufor[MAX_ILOSC_ZNAKOW + 1];
    
    printf("\nPodaj nazwe pliku: ");
	fflush(stdin);
	fgets(bufor, MAX_ILOSC_ZNAKOW, stdin);
    nazwa_pliku = (char*)malloc(sizeof(char)* (strlen(bufor) + 1));
    strncpy(nazwa_pliku, bufor,strlen(bufor) - 1);    
	strcat(nazwa_pliku, ".txt");
	printf("%s",nazwa_pliku);      // \0 \n 
		 
    if (pk->poczatek == NULL)
    	printf("\nNie ma elementow do zapisania");
    else
    {
    	zapisz = fopen(nazwa_pliku, "w");
    	if (zapisz == NULL)
    		printf("\nBlad otwarcia pliku");
    	else
	    {
	    	tmp = pk->poczatek;
	    	while (tmp != NULL)
		    {
		    	fprintf(zapisz, "%s\n", tmp->nazwisko);
		    	tmp = tmp->nastepny;
		    }
		    printf("\nLista zostala zapisana\n");
		}
	    fclose(zapisz);
    }
}
//------------------------------------------------------------------------

p_wezel wyszukaj_kontrahenta(lista *pk, char *nazwisko)
{
    p_wezel pom;
     
    pom = pk->poczatek;
    while (pom != NULL && strcmp(pom->nazwisko, nazwisko) != 0)
    pom = pom->nastepny;
     
    return pom;
}
/*-------------------------------------------------------------------------------*/

void nowa_lista(lista *pk)
{
	p_wezel tmp;
    while (pk->poczatek->nastepny != NULL)
    {
		tmp = pk->poczatek;
		while (tmp->nastepny!= NULL)
	   	{
			tmp = tmp->nastepny;
		}
		zwolnijwezel(tmp);
		tmp->poprzedni->nastepny=NULL;
	}
	pk->poczatek = NULL;
	if(pk->poczatek == NULL)
	{
		printf("\n Lista pusta ");
	}
}

/*-------------------------------------------------------------------------------*/
void usuwa_wybrany_element(lista *pk)
{
    p_wezel tmp, pom;
    char *nazwisko;
    char bufor[MAX_ILOSC_ZNAKOW + 1];
    
    printf("\nPodaj nazwisko: ");
    fflush(stdin);
	fgets(bufor, MAX_ILOSC_ZNAKOW, stdin);
    nazwisko = (char*)malloc(sizeof(char)* (strlen(bufor) + 1));
    strcpy(nazwisko, bufor);
     
    tmp = wyszukaj_kontrahenta(pk, nazwisko);
    if (tmp == NULL)
    printf("\nNie ma takiej osoby");
    else
    {
    	if (tmp == pk->poczatek)
    	{
    		pk->poczatek = tmp->nastepny;
    		zwolnijwezel( tmp );
    	}                                          
    	else
    	{
	    	pom = pk->poczatek;
	    	while ((strcmp(pom->nastepny->nazwisko, tmp->nazwisko) != 0))
	    	pom = pom->nastepny;
	    	if (tmp->nastepny != NULL)
	    	pom->nastepny->poprzedni = tmp->poprzedni;
	    	pom->nastepny = tmp->nastepny;
	    	zwolnijwezel(tmp);
	    }
    }
}     

/* ------------------------------------------------------------------------- */
void sortowanie_listy(lista *pk)//strcmpi str 1 > str 2 = 1
{
	int i=0;
    p_wezel min_value = NULL, current= NULL, iterator= NULL;
    //pk->wybrany=pk->poczatek;
	current = pk->poczatek;

    while (current != NULL)
    {
		iterator = current;
		min_value = iterator;
		//current
		while(iterator->nastepny != NULL)
		{			
			iterator = iterator->nastepny;
			if(0 > strcmp(iterator->nazwisko , min_value->nazwisko))
			{
				min_value = iterator;
			}
		}
		if (min_value != current) {
			//take min pointer, connect neighburs
			min_value->poprzedni->nastepny = min_value->nastepny;
			if (min_value->nastepny){
				min_value->nastepny->poprzedni = min_value->poprzedni;	
			}
			//put min_value before current
			min_value->poprzedni = current->poprzedni;
			
			if (current->poprzedni){
			//point last min value to new min value
			current->poprzedni->nastepny = min_value;
			}
			//current is next for min_value
			min_value->nastepny = current;			
			current->poprzedni = min_value;

		} else {
			current = current->nastepny;
		} 
    }
    //go to begiining of the list
    //int i =0;
    while (pk->poczatek->poprzedni) {
		pk->poczatek = pk->poczatek->poprzedni;
		printf("\n%d", i);
		i++;
    }
}
/* ------------------------------------------------------------------------- */

void wypisanie_listy( lista *pk )   /* wypisz stan listy */
{
  	int i=1; // pomocnicza do policzenia kontrahentow
  	p_wezel  p;
 	p = pk->poczatek;

 	if ( pk->poczatek == NULL )
 	{
		printf("\n Lista pusta");	
	}
  	while(p != NULL)
    {
		printf("\nKontrahent nr: %d", i);
		printf("\nnazwisko: %s", p->nazwisko);
	    p = p->nastepny;
	    i++;
    }
    
  	
  	return;
}
